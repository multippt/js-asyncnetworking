var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var url = require('url');
var WebSocketServer = require('ws').Server;

// Pages
app.use(express.static(__dirname + '/'));
/*app.get('/', function(req, res){
  res.sendFile(__dirname + '/socketio.html');
});
app.get('/socketio', function(req, res){
  res.sendFile(__dirname + '/socketio.html');
});
app.get('/polling', function(req, res){
  res.sendFile(__dirname + '/polling.html');
});
app.get('/longpolling', function(req, res){
  res.sendFile(__dirname + '/longpolling.html');
});
app.get('/websocket', function(req, res){
  res.sendFile(__dirname + '/websocket.html');
});*/

http.listen(3000, function(){
  console.log('listening on *:3000');
});

var messages = []

// Regular polling
app.get('/sync', function(req, res){
	res.send(JSON.stringify(messages));
});

// Long polling
var listeners = []
app.get('/longsync', function(req, res) {
	req.connection.setTimeout(30000);
	var callback = function() {
		try {
			res.send(JSON.stringify(messages));
		} catch (ex) {
		}
	};
	listeners.push(callback);
});

// AJAX send
app.get('/send', function(req, res) {
	var url_parts = url.parse(req.url, true);
	var msg = url_parts.query.message;
	
	// Sync the other clients
	sendMessage(msg);
	res.send(msg);
});

// WebSocket
wss = new WebSocketServer({port:5993});
var broadcastList = []
wss.on('connection', function(ws) {
	broadcastList.push(ws);
	ws.on('message', function(message) {
		var data = JSON.parse(message);
		switch (data.type) {
			case "chatmessage":
				sendMessage(data.data);
				break;
			case "sync":
				var data = {
					type: "sync",
					data: messages
				}
				ws.send(JSON.stringify(data));
				break;
		}
	});
});

// Socket.io
io.on('connection', function(socket){
  console.log('a user connected');
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
  socket.on('sync', function(offset) {
	  // Return to sender
	  socket.emit('sync', messages);
  });
  socket.on('chat message', function(msg){
	// Broadcast to all
    sendMessage(msg);
  });
});

// Broadcast
function sendMessage(msg) {
	messages.push(msg);
	io.emit('chat message', msg);
	
	for (key in listeners) {
        // Send message to listeners
        listeners[key](msg);
    }
    listeners = [];
	
	for (key in broadcastList) {
		try {
			var data = {
				type: "chatmessage",
				data: msg
			}
			broadcastList[key].send(JSON.stringify(data));
		} catch (ex) {
		}
	}
}